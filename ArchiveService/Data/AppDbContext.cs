using Microsoft.EntityFrameworkCore;
using ArchiveService.Models;

namespace ArchiveService.Data
{
    public class AppDbContext : DbContext
    {
        // Pont entre notre model et notre BDD, récupere notre DbContext au niveau du startup
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){}

        public DbSet<Archive> archive { get; set; }
    }
}