using Microsoft.EntityFrameworkCore;
using LikeService.Models;

namespace LikeService.Data
{
    public class AppDbContext : DbContext
    {
        // Pont entre notre model et notre BDD, récupere notre DbContext au niveau du startup
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){}

        public DbSet<Like> like { get; set; }
    }
}