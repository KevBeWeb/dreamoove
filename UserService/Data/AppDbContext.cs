using Microsoft.EntityFrameworkCore;
using UserService.Models;

namespace UserService.Data
{
    public class AppDbContext : DbContext
    {
        // Pont entre notre model et notre BDD, récupere notre DbContext au niveau du startup
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){}

        public DbSet<User> user { get; set; }
    }
}