using Microsoft.EntityFrameworkCore;
using FriendService.Models;

namespace FriendService.Data
{
    public class AppDbContext : DbContext
    {
        // Pont entre notre model et notre BDD, récupere notre DbContext au niveau du startup
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){}

        public DbSet<Friend> friend { get; set; }
    }
}