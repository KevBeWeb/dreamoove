using Microsoft.EntityFrameworkCore;
using ContactService.Models;

namespace ContactService.Data
{
    public class AppDbContext : DbContext
    {
        // Pont entre notre model et notre BDD, récupere notre DbContext au niveau du startup
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){}

        public DbSet<Contact> contact { get; set; }
    }
}