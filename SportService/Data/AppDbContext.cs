using Microsoft.EntityFrameworkCore;
using SportService.Models;

namespace SportService.Data
{
    public class AppDbContext : DbContext
    {
        // Pont entre notre model et notre BDD, récupere notre DbContext au niveau du startup
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){}

        public DbSet<Sport> sport { get; set; }
    }
}