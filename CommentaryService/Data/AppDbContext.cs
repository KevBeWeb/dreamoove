using Microsoft.EntityFrameworkCore;
using CommentaryService.Models;

namespace CommentaryService.Data
{
    public class AppDbContext : DbContext
    {
        // Pont entre notre model et notre BDD, récupere notre DbContext au niveau du startup
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){}

        public DbSet<Commentary> commentary { get; set; }
    }
}